#!/usr/bin/env bash

# This script is used to build fully functional desktop environments, 
# streamed to a browser window in the form of docker containers.
# ---


msg(){ echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"; }
err(){ echo -e "$(tput bold; tput setaf 1)[-] ERROR! ${*}$(tput sgr0)" ; }

n=0
doclist="/tmp/doclist.tmp"
docfile="/tmp/docfile.tmp"
launch="/tmp/launch.tmp"

trap "rm -rf $doclist $docfile $launch" EXIT

images=(
'latest'
'ubuntu-xfce'
'fedora-xfce'
'arch-xfce'
'alpine-kde'
'ubuntu-kde'
'fedora-kde'
'arch-kde'
'alpine-mate'
'ubuntu-mate'
'fedora-mate'
'arch-mate'
'alpine-i3'
'ubuntu-i3'
'fedora-i3'
'arch-i3'
'alpine-openbox'
'ubuntu-openbox'
'fedora-openbox'
'arch-openbox'
'alpine-icewm'
'ubuntu-icewm'
'fedora-icewm'
'arch-icewm'
)

terminate(){ \
clear && echo -e "\n\n" &&
msg "   Please be patient, as current version of this script are terminated..." &&
sudo docker stop $(sudo docker ps -a -q) ; }

makeRunFiles(){ \
for x in "$(cat $docfile)"; do
mkdir -p ./Images/$x &&
echo -e "PASSWORD=$pass1\nUSER=concise\nIMAGE=$x" > ./Images/$x/.env ; 
done ; }

run(){ \
for x in "$(cat $docfile)"; do
msg "  Starting $docfile..."; sleep 1 && 

  port=$(dialog --no-cancel --passwordbox "Please enter a port number (blank for default):" 10 60 3>&1 1>&2 2>&3 3>&1) || port=3000

sudo docker run -d \
   --name=$x-WEB \
   --restart unless-stopped \
   --env-file $PWD/Images/$x/.env \
   -e PUID=1000 \
   -e PGID=1000 \
   -e TZ=Africa/Johannesburg \
   -p $port:3000 \
   -v $PWD/Images/$x:/config \
   lscr.io/linuxserver/webtop:$x ||
   sudo docker start $x-WEB

done ; }
  

whichOS(){ \
for x in "${images[@]}"; do
  echo ""$n"    "$x"" >> /tmp/doclist.tmp
  ((n++)) ; done ; unset n ; unset x

dialog --stdout --no-tags --radiolist  "   Please Select an OS and DE :" 31 35 1 \
  $(cat $doclist|awk '{print $2 " " $2 " " $2}') 2>&1 >$docfile
  echo -e "compose=(\n$(cat $docfile|xargs -n1|sed "s/^/'/g;s/$/'/g")\n)" >$launch ; source $launch
}

password() {
	pass1=$(dialog --no-cancel --inputbox "Please enter a password for the default user:" 10 60 3>&1 1>&2 2>&3 3>&1)
	pass2=$(dialog --no-cancel --inputbox "Retype the password:" 10 60 3>&1 1>&2 2>&3 3>&1)
	while ! [ "$pass1" = "$pass2" ]; do
		unset pass2
		pass1=$(dialog --no-cancel --inputbox "Passwords do not match.\n\nEnter password again." 10 60 3>&1 1>&2 2>&3 3>&1)
		pass2=$(dialog --no-cancel --inputbox "Retype password:" 10 60 3>&1 1>&2 2>&3 3>&1)
	done && clear ; }

terminate ; whichOS ; password 
portno ; makeRunFiles ; run

