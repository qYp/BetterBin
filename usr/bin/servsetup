#!/usr/bin/env bash
##############################################################################################################################
                                                                                                 
                                                  # ##  SERVER SETUP  ## #
                                                                                                                               
##                                    A short script to quickly setup mysql and mariadb                                     ##
##                                        servers to save time on a clean install.                                          ##
                                                                                                                              
##                                   NOTE : This script is targeted at Arch Linux systems,                                  ##
##                                   and not debian, and only installes the bare minimum.                                   ##
##                                   Customisation and further tasks are left to the user.                                  ##


##############################################################################################################################
##############################################################################################################################



color1="blue" ; color2="pink" ; color3="yellow"


msg(){ echo -e "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }

err(){ echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)" && exit 1337; }


[ -n $(echo $color1) ] || [ ! $color1 ] && color=blue

[ -n $(echo $color2) ] || [ ! $color2 ] && color=pink

[ -n $(echo $color3) ] || [ ! $color3 ] && color=yellow


[ "$(id -u)" -ne 0 ] && err "root priviledges are required to run this script."



yellow(){ echo -n  "$(tput bold; tput setaf 3)${*}$(tput sgr0)" ; }

blue()  { echo -n  "$(tput bold; tput setaf 6)${*}$(tput sgr0)" ; }

white() { echo -n  "$(tput bold; tput setaf 7)${*}$(tput sgr0)" ; }

pink()  { echo -n  "$(tput bold; tput setaf 5)${*}$(tput sgr0)" ; }

red()   { echo -n  "$(tput bold; tput setaf 1)${*}$(tput sgr0)" ; }

green() { echo -n  "$(tput bold; tput setaf 2)${*}$(tput sgr0)" ; }

random(){ echo -n  "$(tput bold; tput setaf "$(seq 25|shuf|head -1)") ${*}$(tput sgr0)" ; }

err()   { echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)" && exit 1337 ; }



 SETUP_SERVER(){ \

name="$(dialog --inputbox "  Please Enter the Name of the User \
Intended to Manage the Server (not the root user) :  " \
10 95 3>&1 1>&2 2>&3 3>&1)" && clear && echo -e "\n" ; 
[ -z $name ] && clear && echo -e "\n\n" && err " User Exited."

sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql &&
mysql_secure_installation -u root

sudo systemctl enable --now mysqld.service
sudo systemctl enable --now nginx.service
sudo systemctl enable --now mariadb.service
sudo systemctl enable --now php-fpm.service 

! [ -h /home/$name/WWW ] && sudo ln -sf /usr/share/nginx/html \
/home/$name/WWW ; sudo chown $name:$name /usr/share/nginx/ -R >/dev/null 2>&1
 
cat <<EOF >/usr/share/nginx/html/index.html
<!DOCTYPE html>
<html><head><title>Concise Server Setup</title><style>
  body { width: 35em;
         margin: 0 auto;
         font-family: Tahoma, Verdana, Arial, sans-serif; }
</style></head><body><h1>Welcome to Your Server!  Have fun :)</h1><h3> - dqYpb </h3>
<p><em>        Thank you for using<b> Concise Server Setup!</b></em></p>
</body></html>
EOF

 }

  packages=(

',certbot,A tool to automatically receive and install X.509 certificates to enable TLS on servers.'
',certbot-nginx,Nginx plugin for Let’s Encrypt client.'
',archiso,A collection of bash scripts allowing for one to build up to date ArchIso images.'
',mkinitcpio,A tool used by the archiso script.'
',mkinitcpio-archiso,Used by archiso.'
',php,general-purpose scripting language that is especially suited to web development.'
',php-cgi,CGI and FCGI SAPI for PHP.'
',php-gd,gd module for PHP.'
',php-fpm,FastCGI Process Manager for PHP.'
',php-intl,intl module for PHP.'
',nginx,A high performance web server.'
',mysql,A popular open source relational database system.'
',mariadb,A popular open source relational database system.'
',ufw,A firewall and security tool.'

 )


GET_PACKAGES(){ \

npminstall() { sudo -u "$name" npm install --save "$1" "$npmd"/"$pkg" >/dev/null 2>&1 ; }

scrstart()   { clear ; echo -e "\n\n" ; green "    Downloading Packages : " ; echo  -e "\n\n" ; }

snpinstall() { sudo -u "$name" snap install "$1" "$snpd"/"$pkg" >/dev/null 2>&1 ; }

pkginstall() { paru -S --needed --noconfirm "$1" >/dev/null 2>&1 ; }

pipinstall() { sudo -u "$name" python3 -m pip install "$1" "$pipd"/"$pkg" >/dev/null 2>&1 ; }


labclone()   { git clone https://gitlab.com/"$1".git \
    "$labd"/"$pkg" >/dev/null 2>&1 ; }

mylabclone() { git clone https://gitlab.com/"$labname"/"$(basename $1)".git \
    "$gitd"/"$(basename $pkg)" >/dev/null 2>&1 ; }

myhubclone() { git clone https://github.com/"$hubname"/"$(basename $1)".git \
    "$gitd"/"$(basename $pkg)" >/dev/null 2>&1 ; }

hubclone()   { git clone https://github.com/"$1".git \
    "$hubd"/"$pkg" >/dev/null 2>&1 ; }


pkgd="$HOME/PKGDownloads"        ;  labd="$HOME/PKGDownloads/GitLab"     

pipd="$HOME/PKGDownloads/PIP"    ;  hubd="$HOME/PKGDownloads/GitHub"          

snpd="$HOME/PKGDownloads/Snaps"  ;  gitd="$HOME/PKGDownloads/MyGitRepos"   

npmd="$HOME/PKGDownloads/NPM"    ;  


[ -f /tmp/total.tmp ] && rm /tmp/total.tmp

for x in  "${packages[@]}" ; do  echo $x >> /tmp/total.tmp ; 

sed -i '/^\s/d;/^#/d' /tmp/total.tmp ; done


for d in "/tmp/total.tmp" ; do  
    
  src="$(cat $d|cut -d',' -f1 -s)" 
 
  pkg="$(cat $d |cut -d',' -f2)"   
    
  desc="$(cat $d|cut -d',' -f3)"   

done ; scrstart



while IFS=, read -r src pkg desc ; do

  ((n=$n+1));total=$(wc -l < /tmp/total.tmp) ; 
  
  $color1 "( $n of $total ) " ; $color2 $pkg && 
  
    [ -n "$(echo "$desc")" ] && green " - " && 
    
    $color3 "$desc" ; echo ""
    
for x in "$src" ; do pkginstall "$pkg" ; done ; 

done < /tmp/total.tmp ; }



 START_SETUP(){ \

for pkg in "/tmp/total.tmp" ; do  
    
  pkg="$(cat $pkg |cut -d',' -f2)"   

 for x in "${pkg[@]}"; do ! sudo pacman -Qq $x >/dev/null 2>&1 && GET_PACKAGES 

  break ; ufw allow 80 443 'www full' www http https >/dev/null 2>&1 ;  

done ; done ; }


  
  START_SETUP && echo -e "\n" && SETUP_SERVER &&
  
  clear ; echo -e "\n\n" ; msg "  Congratulations! Your Server is Successfully setup, configured and ready for use!" && 

  sleep 2 ; echo "" ; blue "  A link to the live network folder has been created in your home directory as 'WWW'." ; sleep 3 &&

  echo -e "\n" ; pink "  Visit your server at : 127.0.0.1 - open port 80 on your router to make your server publicly accessible." ; 

  sleep 2 && echo -e "\n" ; exit 0 || err "  Setup Failed"


